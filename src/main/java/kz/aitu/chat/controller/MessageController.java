package kz.aitu.chat.controller;


import kz.aitu.chat.model.Message;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/message")
@AllArgsConstructor
public class MessageController {
    private MessageService messageService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(messageService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        messageService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message) {
        return ResponseEntity.ok(messageService.save(message));
    }

    @GetMapping("/get10ByChatId/{id}")
    public ResponseEntity<?> get10ByChatId(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.getLast10ByChatId(id));
    }

    @GetMapping("get10ByUserId/{id}")
    public ResponseEntity<?> get10ByUserId(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.getLast10ByUserId(id));
    }

    @GetMapping("/{chatId}/unread")
    public ResponseEntity<?> getUndeliveredMessages(@PathVariable Long chatId, @RequestParam Long userId) {
        try {
            return ResponseEntity.ok(messageService.getAllUndelivered(chatId, userId));
        } catch (Exception e) {
            return ResponseEntity.ok("Error");
        }
    }
}
