package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/v1/chat")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(chatService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        chatService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat) {
        return ResponseEntity.ok(chatService.save(chat));
    }


    @GetMapping("/{chatId}/users")
    public ResponseEntity<?> getUsersByChatId(@PathVariable Long chatId) {
        return ResponseEntity.ok(chatService.getUsersByChatId(chatId));
    }

    @GetMapping("read-chat/{chatId}")
    public ResponseEntity<?> readChatByChatId(@PathVariable Long chatId, @RequestParam Long reader_id) throws Exception {
        return ResponseEntity.ok(chatService.readChat(chatId, reader_id));
    }

}