package kz.aitu.chat.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "message")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    private long userId;
    private long chatId;
    private String text;
    private long created_timestamp;
    private long updated_timestamp;
    private boolean isDelivered;
    private long deliveredTimestamp;
    private boolean isRead;
    private long readTimestamp;
    private String messageType;

}
