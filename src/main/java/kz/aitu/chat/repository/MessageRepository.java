package kz.aitu.chat.repository;

import kz.aitu.chat.model.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> getLast10ByChatId(Long chatId);

    List<Message> getLast10ByUserId(Long userId);

    List<Message> findAllDeliveredUnread(Long chatId, Long userId);

    List<Message> findAllUndelivered(Long chatId, Long userId);
}
