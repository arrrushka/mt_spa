package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository chatRepository;
    private ParticipantService participantService;
    private UsersService usersService;
    private MessageService messageService;

    public List<Chat> findAll() {
        return chatRepository.findAll();
    }

    public Optional<Chat> findById(Long id) {
        return chatRepository.findById(id);
    }

    public Chat save(Chat chat) {
        return chatRepository.save(chat);
    }

    public void deleteById(Long id) {
        chatRepository.deleteById(id);
    }


    public List<Users> getUsersByChatId(Long chat_id) {
        List<Participant> participantList = participantService.findAllByChatId(chat_id);
        List<Users> userList = new ArrayList<>();
        for (Participant participant : participantList) {
            Optional<Users> usersOptional = usersService.findById(participant.getUserId());
            if (usersOptional.isPresent()) {
                userList.add(usersOptional.get());
            }
        }
        return userList;
    }

    public List<Message> readChat(Long chatId, Long readerId) throws Exception {
        if (!participantService.isExists(chatId, readerId)) {
            throw new Exception("Doesn't exist");
        }
        List<Message> notReadMessages = messageService.getUnread(chatId, readerId);
        for (Message message : notReadMessages) {
            message.setRead(true);
            message.setReadTimestamp(new Date().getTime());
            messageService.save(message);
        }
        return notReadMessages;
    }
}
