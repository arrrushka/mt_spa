package kz.aitu.chat.service;


import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UsersService {
    private UsersRepository usersRepository;

    public List<Users> findAll() {
        return usersRepository.findAll();
    }

    public Optional<Users> findById(Long id) {
        return usersRepository.findById(id);
    }

    public Users save(Users users) {
        return usersRepository.save(users);
    }

    public void deleteById(Long id) {
        usersRepository.deleteById(id);
    }
}
