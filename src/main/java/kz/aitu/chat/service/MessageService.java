package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;
    private ParticipantService participantService;

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public Optional<Message> findById(Long id) {
        return messageRepository.findById(id);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

    public Message changeDate(Message message) {
        Date date = new Date();
        message.setCreated_timestamp(date.getTime());
        return messageRepository.save(message);
    }

    public List<Message> getLast10ByChatId(Long id) {
        return messageRepository.getLast10ByChatId(id);
    }

    public List<Message> getLast10ByUserId(Long id) {
        return messageRepository.getLast10ByUserId(id);
    }

    public List<Message> getUnread(Long chatId, Long userId) throws Exception {
        if (!participantService.isExists(chatId, userId)) {
            throw new Exception("Doesn't exist");
        }
        return messageRepository.findAllDeliveredUnread(chatId, userId);
    }

    public List<Message> getAllUndelivered(Long chatId, Long userId) throws Exception {
        if (!participantService.isExists(chatId, userId)) {
            throw new Exception("Doesn't exist");
        }
        return messageRepository.findAllUndelivered(chatId, userId);
    }
}
