package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Chat;
import kz.aitu.chat.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private AuthRepository authRepository;


    public boolean login(String login, String password) {
        Auth auth = authRepository.findByLoginAndPassword(login, password);
        if (auth == null) {
            return false;
        } else {
            auth.setLastLoginTimeStamp(new Date().getTime());
            auth.setToken(UUID.randomUUID().toString());
            add(auth);
            return true;
        }
    }

    public boolean isTokenExist(String t) {
        if (authRepository.isExistsByToken(t)) {
            return true;
        }
        return false;
    }

    public void add(Auth auth) {
        authRepository.save(auth);
    }

    public List<Auth> findAll() {
        return authRepository.findAll();
    }

    public Optional<Auth> findById(Long id) {
        return authRepository.findById(id);
    }

    public Auth save(Auth auth) {
        return authRepository.save(auth);
    }

    public void deleteById(Long id) {
        authRepository.deleteById(id);
    }
}
