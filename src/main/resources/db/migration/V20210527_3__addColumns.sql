alter table message
    add column is_read boolean,
    add column read_timestamp bigint,
    add column is_delivered boolean,
    add column delivered_timestamp bigint;
